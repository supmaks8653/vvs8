#ifndef FILTER_H
#define FILTER_H

#include "bank_operations.h"

operation** filter(operation* array[], int size, bool (*check)(operation* element), int& result_size);
bool check_november_2021(operation* element);
bool check_in_operations(operation* element);

#endif
