#include<iostream>
#include "bank_operations.h"
#include "file_reader.h"
#include "filter.h"
using namespace std;
void output(operation* operation) {
    /*********����� ���� ��������*********/
    cout << "���� ��������...: ";
    //����� ��� ��������
    cout << operation->date.day << '.';
    //����� ������ ��������
    cout << operation->date.month << '.';
    //����� ���� ��������
    cout << operation->date.year << '\n';
    /*********����� ������� ��������*********/
    cout << "����� ��������...: ";
    //����� ���� ��������
    cout << operation->time.hour << ':';
    //����� ������ ��������
    cout << operation->time.minute << ':';
    //����� ������� ��������
    cout << operation->time.second << '\n';
    //����� ���� ��������
    cout << "��� ��������: " << operation->operationType << '\n';
    //����� �����, ���������� � ���������
    cout << "�������� ������� �� ������ ����� " << operation->cardNum << '\n';
    //����� ����� ��������
    cout << "����� ��������: " << operation->amount << '\n';
    //����� ���������� ��������
    cout << "���������� ��������: " << operation->purpose << "\n\n";
}
int main() {
	setlocale(LC_ALL, "Russian");
	cout << "Laboratory work #8. GIT\n";
    cout << "Variant #7. Bank operations\n";
    cout << "Author: Maksim Matveenko\n";
    cout << "Group: 11\n";
	operation* operations[MAX_FILE_ROWS_COUNT];
	int size;
	try {
        read("data.txt", operations, size);
        cout << "***** �������� �� ����� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(operations[i]);
        }
		bool (*check_function)(operation*) = NULL;
		cout << "\n��������� ��������:\n";
		cout << "1)����������� ��������, ��������� � ������ 2021 ����\n";
		cout << "2)����������� ���������� ��������\n";
        cout << "��� �����: ";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_november_2021;      
			cout << "***** ��������, ��������� � ������ 2021 ���� *****\n\n";
			break;
		case 2:
			check_function = check_in_operations;      
			cout << "***** ���������� �������� *****\n\n";
			break;
		default:
			throw "�� ���������� �������� � ����� �������.\n";
		}
		if (check_function)
		{
            int new_size;
			operation** filtered = filter(operations, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
        for (int i = 0; i < size; i++)
        {
            delete operations[i];
        }
	}
    catch (const char* error)
    {
        cout << error << '\n';
    }
	system("pause");
	return 0;
}
